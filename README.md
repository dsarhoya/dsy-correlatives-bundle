dsy-correlatives-bundle
=======================

Para las configurar el servicio ofrecido se debe hacer de la siguiente manera en el archivo `./app/config/config.yml`:

```
dsy_correlatives:
    initial_correlative: <correlativo inicial>
    context_initial_correlatives:
        - { context: <contexto a definir>, initial_correlative: <correlativo inicial del contexto> }
        - { context: <contexto a definir>, initial_correlative: <correlativo inicial del contexto> }
```

No es necesario realizar esta configuración si es que los valores deseados comienzan desde cero.

Tags:

* 0.0.12: Nuevo commando para ejecutar la function de AggregateContext. Bug: pasando la propidad context a text en la configuración del bundle

A Symfony project created on January 31, 2019, 2:21 pm.


