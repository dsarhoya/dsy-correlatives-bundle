<?php

namespace AppBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use dsarhoya\DSYCorrelativesBundle\Service\CorrelativeService;
use Symfony\Component\Console\Input\InputOption;

class AppTestCorrelativeCommand extends Command
{
    private $correlativeService;

    public function __construct(CorrelativeService $correlativeService)
    {
        $this->correlativeService = $correlativeService;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('app:test:correlative')
            ->setDescription('...')
            ->addArgument('context', InputArgument::REQUIRED, 'It should be a integer')
            ->addOption('count', 'c', InputOption::VALUE_OPTIONAL, 'Cantidad de correlativos')
            ->addOption('aggregate', 'a', InputOption::VALUE_NONE, 'Agregar o no')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $context = $input->getArgument('context');
        $count = null === $input->getOption('count') ? 1 : (int)$input->getOption('count');

        for ($i=0; $i < $count; $i++) { 
            $correlative = $this->correlativeService->createCorrelative($context);
            dump($correlative);
        }
        
        if (true === $input->getOption('aggregate')) {
            $this->correlativeService->aggregateContext($context);
        }

        // dump([
        //     'context' => $context,
        // ]);

        // die;
    }
}
