<?php

namespace dsarhoya\DSYCorrelativesBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;
use Symfony\Component\Config\Definition\Builder\NodeDefinition;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        /** @var ArrayNodeDefinition|NodeDefinition $rootNode */
        $rootNode = $treeBuilder->root('dsy_correlatives');

        $fn = 'arrayPrototype';
        $param = null;

        if (!method_exists($rootNode->children()->arrayNode('context_initial_correlatives'), 'arrayPrototype')) {
            $fn = 'prototype';
            $param = 'array';
        }

        $rootNode
            ->children()
                ->integerNode('initial_correlative')
                    ->min(0)
                    ->defaultValue(0)
                ->end()
                ->arrayNode('context_initial_correlatives')
                    ->$fn($param)
                        ->children()
                            ->scalarNode('context')->end()
                            ->integerNode('initial_correlative')->end()
                        ->end()
                    ->end()
                ->end()
            ->end();

        return $treeBuilder;
    }
}
