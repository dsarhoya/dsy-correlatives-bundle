<?php

namespace dsarhoya\DSYCorrelativesBundle\Service;

use Doctrine\ORM\EntityManagerInterface;
use dsarhoya\DSYCorrelativesBundle\Entity\CorrelativeAggregation;
use dsarhoya\DSYCorrelativesBundle\Entity\CorrelativeIdentifier;
use dsarhoya\DSYCorrelativesBundle\Repository\CorrelativeIdentifierRepository;

class CorrelativeService
{
    private $em;
    private $initialCorrelative;
    private $customCorrelatives;

    public function __construct(EntityManagerInterface $em, $initialCorrelative, $customCorrelatives)
    {
        $this->em = $em;
        $this->initialCorrelative = $initialCorrelative;
        $this->customCorrelatives = $customCorrelatives;
    }

    public function createCorrelative($context)
    {
        $correlativeIdentifier = new CorrelativeIdentifier();
        $correlativeIdentifier->setContext($context);

        $this->em->persist($correlativeIdentifier);
        $this->em->flush($correlativeIdentifier);

        $aggregatedCount = 0;
        if (null !== $aggregation = $this->em->getRepository(CorrelativeAggregation::class)->findOneBy(['context' => $context])) {
            $aggregatedCount = $aggregation->getAggregatedCount();
        }

        /** @var CorrelativeIdentifierRepository $repo */
        $repo = $this->em->getRepository(CorrelativeIdentifier::class);
        $previousCorrelativeCount = $repo->countPreviousCorrelatives($correlativeIdentifier);

        $this->em->persist($correlativeIdentifier);
        $this->em->flush($correlativeIdentifier);

        $offset = $this->getOffset($context);

        return $offset + $aggregatedCount + $previousCorrelativeCount + 1;
    }

    private function getOffset($context)
    {
        $match = array_filter(
            $this->customCorrelatives,
            function ($pairConfig) use ($context) { return $pairConfig['context'] == $context; }
        );

        if (empty($match)) {
            return $this->initialCorrelative;
        } else {
            return array_values($match)[0]['initial_correlative'];
        }

        return $this->initialCorrelative;
    }

    public function aggregateContext($context)
    {
        /*
         * Esto es importante si no, la consulta abajo queda expuesta a inyecci�n sql.
         */
        if (1 !== preg_match(CorrelativeIdentifier::ALLOWED_CHARACTERS_REGEX, $context)) {
            throw new \Exception('DSY Correlatives bundle: un contexto solo permite a-z A-Z 0-9 - _ .');
        }

        if (null === $this->em->getRepository(CorrelativeAggregation::class)->findOneBy(['context' => $context])) {
            $aggregation = new CorrelativeAggregation();
            $aggregation->setContext($context);
            $aggregation->setAggregatedCount(0);
            $this->em->persist($aggregation);
            $this->em->flush($aggregation);
        }

        /**
         * Esto lo hago con la api nativa de MYSQL para poder tirar las dos consultas seguidas.
         */
        $sql = "
            update correlative_aggregation
                set aggregatedCount = aggregatedCount + (
                    select count(id) from correlative_identifier where context = '$context'
                )
                where context = '$context';
            delete from correlative_identifier where context = '$context';
        ";

        $this->em->getConnection()->exec($sql);
    }

    public function setCurrentCorrelative($context, $correlative)
    {
        if (null == $aggregation = $this->em->getRepository(CorrelativeAggregation::class)->findOneBy(['context' => $context])) {
            $aggregation = new CorrelativeAggregation();
            $aggregation->setContext($context);
            $this->em->persist($aggregation);
        }

        $aggregation->setAggregatedCount($correlative);
        $identifiers = $this->em->getRepository(CorrelativeIdentifier::class)->findBy(['context' => $context]);
        foreach ($identifiers as $identifier) {
            $this->em->remove($identifier);
        }
        $this->em->flush();
    }

    public function getCurrentCorrelative($context)
    {
        $aggregatedCount = 0;
        if (null !== $aggregation = $this->em->getRepository(CorrelativeAggregation::class)->findOneBy(['context' => $context])) {
            $aggregatedCount = $aggregation->getAggregatedCount();
        }

        $identifiers = $this->em->getRepository(CorrelativeIdentifier::class)->findByContext($context);

        $offset = $this->getOffset($context);

        return $offset + $aggregatedCount + count($identifiers);
    }
}
