<?php

namespace dsarhoya\DSYCorrelativesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CorrelativeIdentifier.
 *
 * @ORM\Table(
 *  name="correlative_identifier",
 *  indexes={@ORM\Index(name="context_index", columns={"context"})}
 * )
 * @ORM\Entity(repositoryClass="dsarhoya\DSYCorrelativesBundle\Repository\CorrelativeIdentifierRepository")
 */
class CorrelativeIdentifier
{
    const ALLOWED_CHARACTERS_REGEX = "/^[a-zA-Z0-9-_\.]+$/";

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="context", type="string", length=200)
     */
    private $context;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $createdAt;

    public function __construct()
    {
        $this->setCreatedAt(new \DateTime('now'));
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set context.
     *
     * @param int $context
     *
     * @return CorrelativeIdentifier
     */
    public function setContext($context)
    {
        if (1 !== preg_match(self::ALLOWED_CHARACTERS_REGEX, $context)) {
            throw new \Exception('DSY Correlatives bundle: un contexto solo permite a-z A-Z 0-9 - _ .');
        }

        $this->context = $context;

        return $this;
    }

    /**
     * Get context.
     *
     * @return int
     */
    public function getContext()
    {
        return $this->context;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return CorrelativeIdentifier
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
}
