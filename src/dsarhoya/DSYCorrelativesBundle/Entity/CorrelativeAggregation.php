<?php

namespace dsarhoya\DSYCorrelativesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CorrelativeAggregation
 *
 * @ORM\Table(name="correlative_aggregation")
 * @ORM\Entity(repositoryClass="dsarhoya\DSYCorrelativesBundle\Repository\CorrelativeAggregationRepository")
 */
class CorrelativeAggregation
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="context", type="string", length=200, unique=true)
     */
    private $context;

    /**
     * @var int
     *
     * @ORM\Column(name="aggregatedCount", type="integer")
     */
    private $aggregatedCount;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set context
     *
     * @param string $context
     *
     * @return CorrelativeAggregation
     */
    public function setContext($context)
    {
        $this->context = $context;

        return $this;
    }

    /**
     * Get context
     *
     * @return string
     */
    public function getContext()
    {
        return $this->context;
    }

    /**
     * Set aggregatedCount
     *
     * @param integer $aggregatedCount
     *
     * @return CorrelativeAggregation
     */
    public function setAggregatedCount($aggregatedCount)
    {
        $this->aggregatedCount = $aggregatedCount;

        return $this;
    }

    /**
     * Get aggregatedCount
     *
     * @return int
     */
    public function getAggregatedCount()
    {
        return $this->aggregatedCount;
    }
}

