<?php

namespace dsarhoya\DSYCorrelativesBundle\Command;

use Doctrine\ORM\EntityManagerInterface;
use dsarhoya\DSYCorrelativesBundle\Entity\CorrelativeIdentifier;
use dsarhoya\DSYCorrelativesBundle\Repository\CorrelativeIdentifierRepository;
use dsarhoya\DSYCorrelativesBundle\Service\CorrelativeService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * AggregateContext Command.
 *
 * @author snake77se <yosip.curiel@dsarhoya.cl>
 */
class AggregateContextCommand extends Command
{
    protected static $defaultName = 'dsy:correlative:aggregate-context';
    protected $correlativeSrv;
    protected $em;

    /**
     * Constructor.
     */
    public function __construct(CorrelativeService $correlativeSrv, EntityManagerInterface $em)
    {
        parent::__construct();
        $this->correlativeSrv = $correlativeSrv;
        $this->em = $em;
    }

    protected function configure()
    {
        $this
            ->setName('dsy:correlative:aggregate-context')
            ->addArgument('context', InputArgument::OPTIONAL, 'context')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $dateString = (new \DateTime())->format('Y-m-d H:i:s');

        /** @var CorrelativeIdentifierRepository $repo */
        $repo = $this->em->getRepository(CorrelativeIdentifier::class);

        $contexts = [];

        $contextName = $input->getArgument('context');

        if (null !== $contextName) {
            $contexts[] = $contextName;
        }

        if (!count($contexts)) {
            $contexts = $repo->getContextList();
        }

        if (!count($contexts)) {
            $output->writeln("[$dateString] no contexts found");

            return;
        }

        foreach ($contexts as $c) {
            $this->correlativeSrv->aggregateContext($c);
        }

        $output->writeln("[$dateString] done");
    }
}
